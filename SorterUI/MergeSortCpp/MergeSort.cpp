#include <thread>
#include <future>
#include <list>
#include "pch.h"
#include "MergeSort.h"
#include <iostream>
#include <string>

/*
 Topic: Merge Sort algorithm - execution time comparison using cpp and dll code
 Description: Program sorts randomly generated data using Merge Sort algorithms written in cpp and asm. Its size is determined by the user.
 02.11.2020, 5th semester, year 2020/2021, Bartosz Groffik
 Current version: included in PZE folder
 Previous versions' functionalities:
 02.11.2020 - final version
 30.10.2020 - Refactoring and final version of UI
 29.10.2020 - Asm algorithm was created, optimized array filling and array containers
 28.10.2020 - 1st version of .asm project was created
 26.10.2020 - Universal data and container generation was added
 25.10.2020 - Data results moved to C# and resolved dll issues
 15.10.2020 - Merge sort C++ implementation was created
 12.10.2020 - 1st version of user UI was created
*/

using namespace std;

// DLL internal state variables:
static unsigned long long previous_;  // Previous value, if any
static unsigned long long current_;   // Current sequence value
static unsigned index_;               // Current seq. position

//Function comparing elements of given arrays and assigning them into the array
void merge(int arr[], int l, int m, int r)
{
    int n1 = m - l + 1;
    int n2 = r - m;

    // Create temp arrays 
    int* L = new int[n1];
    int* R = new int[n2];

    // Copy data to temp arrays L[] and R[] 
    for (int i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (int j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    // Merge the temp arrays back into arr[l..r]

    // Initial index of first subarray
    int i = 0;

    // Initial index of second subarray
    int j = 0;

    // Initial index of merged subarray
    int k = l;

    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    // Copy the remaining elements of
    // L[], if there are any 
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    // Copy the remaining elements of
    // R[], if there are any 
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }

    delete[] L;
    delete[] R;
}

// l is for left index and r is 
// right index of the sub-array
// of arr to be sorted */
//Function splitting the array into smaller arrays and merging them
void mergeSort(int arr[], int l, int r)
{
    if (l < r)
    {

        // Same as (l+r)/2, but avoids 
        // overflow for large l and h
        int m = l + (r - l) / 2;

        // Sort first and second halves
        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);

        merge(arr, l, m, r);
    }
}

