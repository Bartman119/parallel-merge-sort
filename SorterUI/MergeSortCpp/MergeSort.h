#pragma once
#ifdef MERGESORT_EXPORTS
#define MERGESORT_API __declspec(dllexport)
#else
#define MERGESORT_API __declspec(dllimport)
#endif

#include<string>

extern "C" MERGESORT_API void merge(int arr[], int l, int m, int r);

extern "C" MERGESORT_API void mergeSort(int arr[], int l, int r);

