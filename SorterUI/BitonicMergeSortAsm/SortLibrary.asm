
;Topic: Merge Sort algorithm - execution time comparison using cpp and dll code
;Description: Program sorts randomly generated data using Merge Sort algorithms written in cpp and asm. Its size is determined by the user.  
;02.11.2020, 5th semester, year 2020/2021, Bartosz Groffik
;Current version: included in PZE folder
;Previous versions' functionalities:
;02.11.2020 - final version
;30.10.2020 - Refactoring and final version of UI
;29.10.2020 - Asm algorithm was created, optimized array filling and array containers
;28.10.2020 - 1st version of .asm project was created
;26.10.2020 - Universal data and container generation was added
;25.10.2020 - Data results moved to C# and resolved dll issues
;15.10.2020 - Merge sort C++ implementation was created
;12.10.2020 - 1st version of user UI was created

.data
counter dd 3

.code
;Procedure allows for merge sort of two 4 integer arrays using bitonic mergesort approach that is achieved using SIMD instructions
mergeSortASM proc 
	mov counter, 3
	movdqu xmm0, xmmword ptr [rcx]	; move first vector
	movdqu xmm1, xmmword ptr [rdx]	; move second vector
	


LoopHead:

	movdqu xmm2, xmm0				; store value of xmm0 in xmm2 (1st vector)

	pmaxud xmm0, xmm1				; get maximum values of both vectors to xmm0
	pminud xmm1, xmm2				; get minimum values of both vectors to xmm1

									;currently xmm0->max, xmm1->min, xmm2->previous xmm0

	movdqu xmm2, xmm1				; store value of xmm1 in xmm2 (2nd vector)
	punpckhdq xmm1, xmm0			; unpack higher bits of data and store them in xmm1
	punpckldq xmm2, xmm0			; unpack lower bits of data and store them in xmm2
	movdqu xmm0, xmm2				; move value of 2nd vector to xmm0

									;currently xmm0-> 1st vector new interation, xmm1 -> 2nd vector new iteration
									;xmm2 -> 2nd vector new iteration

									;repeat vectorSize-1 times
	dec counter
	jnz LoopHead

movdqu xmmword ptr [rcx], xmm0		; move lower vector 
movdqu xmmword ptr [rdx], xmm1		; move higher vector
ret

mergeSortASM endp

end