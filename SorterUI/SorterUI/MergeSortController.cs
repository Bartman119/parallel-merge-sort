﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Text;
using System.Linq;


/*
 Topic: Merge Sort algorithm - execution time comparison using cpp and dll code
 Description: Program sorts randomly generated data using Merge Sort algorithms written in cpp and asm. Its size is determined by the user.  
 02.11.2020, 5th semester, year 2020/2021, Bartosz Groffik
 Current version: included in PZE folder
 Previous versions' functionalities:
 02.11.2020 - final version
 30.10.2020 - Refactoring and final version of UI
 29.10.2020 - Asm algorithm was created, optimized array filling and array containers
 28.10.2020 - 1st version of .asm project was created
 26.10.2020 - Universal data and container generation was added
 25.10.2020 - Data results moved to C# and resolved dll issues
 15.10.2020 - Merge sort C++ implementation was created
 12.10.2020 - 1st version of user UI was created
*/
namespace SorterUI
{
    class MergeSortController
    {
        //Class used to import functions from cpp dll
        public unsafe class SortCpp
        {
            [DllImport("MergeSortCpp.dll", CharSet = CharSet.Ansi, SetLastError = true)]
            public static extern int getMessageLength(int[] arr, int arr_size);

            [DllImport("MergeSortCpp.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void mergeSort(int[] arr, int l, int r);

        }

        //Class used to import a function from asm dll
        public unsafe class SortAsm
        {
            [DllImport("BitonicMergeSortAsm.dll")]
            public static extern void mergeSortASM(int[] x, int[] y);
        }

        //maximum integer value of randomized numbers
        public int _INT_MAX;
        
        //size of the cpp array
        public int _ARRAY_SIZE;

        //number of elements to generate given by the user
        public int _NumberOfIntegers;

        //size of the asm array
        public int arr_size;

        //complete number of integers with potential added zeroes to make data divisible by 8
        public int FinalNumberOfIntegers;
        
        //array container for cpp calculations
        public int[] CppArr;
        
        //first array container for asm calculations
        public int[] FirstAsmArr;

        //second array container for asm calculations
        public int[] SecondAsmArr;

        //array containing results of sorting
        public int[] result;

        //MergeSortController class constructor, used to execute asm and cpp sorting algorithms
        public MergeSortController(int INT_MAX, int ARRAY_SIZE, int NumberOfIntegers) 
        {
            _INT_MAX = INT_MAX;
            _ARRAY_SIZE = ARRAY_SIZE;
            _NumberOfIntegers = NumberOfIntegers;
        }

        //Function used to generate random values used for sorting
        public void GenerateValues()
        {
            Random randInt = new Random();
            //List container used to temporarily hold generated values
            List<int> cppList = new List<int>();
            int counter = 0;
            int offset = 0;
            //array holding each pack of data used for comparisons in simd in asm
            for (int i = 0; i < _NumberOfIntegers; i++)
            {
                if (counter % _ARRAY_SIZE == 0)
                    counter = 0;

                //generate pseudorandom value
                int generatedValue = randInt.Next(_INT_MAX);
                //add it to cppList
                cppList.Add(generatedValue);
                //if it's the last int add enough elements to make their count divisible by 8
                if (i == _NumberOfIntegers - 1)
                {
                    while (counter < _ARRAY_SIZE - 1)
                    {
                        cppList.Add(0);
                        counter++;
                        offset++;
                    }
                }
                counter++;
            }
            SetArraySizes(_NumberOfIntegers, offset);
            FillDataContainers(cppList);
        }

        //Function initializing arrays depending on the amount of values generated
        public void SetArraySizes(int NumberOfIntegers, int offset)
        {
            FinalNumberOfIntegers = NumberOfIntegers + offset;
            CppArr = new int[FinalNumberOfIntegers];
            FirstAsmArr = new int[FinalNumberOfIntegers / 2];
            SecondAsmArr = new int[FinalNumberOfIntegers / 2];
        }

        //Function passing data to created arrays
        public void FillDataContainers(List<int> cppList)
        {
            int elementCounter = 0;

            for (int i = 0; i < FinalNumberOfIntegers / 2; i++)
            {
                CppArr[elementCounter] = cppList.ElementAt(elementCounter);
                FirstAsmArr[i] = cppList.ElementAt(elementCounter);
                elementCounter++;

                CppArr[elementCounter] = cppList.ElementAt(elementCounter);
                SecondAsmArr[i] = cppList.ElementAt(elementCounter);
                elementCounter++;
            }
        }

        //Function sorting arrays using ASM code
        public void SortUsingASM()
        {
            //sort both arrays (both pre sorted)
            arr_size = FirstAsmArr.Length;
            //sort first array in ascending order (used to pre-sort two smaller data containers in order to execute bitonic sort)
            SortCpp.mergeSort(FirstAsmArr, 0, arr_size - 1);
            //sort second array in ascending order
            SortCpp.mergeSort(SecondAsmArr, 0, arr_size - 1);

            int arrayIndexThreshold = 4;
            //index for result arr traversal
            int currentIndex = 0;
            //index for given sub arr traversal
            int currentSubIndex = 0;
            //index for next element to be checked in first array  
            int FirstArrIndex = 0;
            //index for next element to be checked in second array
            int SecondArrIndex = 0;
            int[] FirstSubArr = new int[4];
            int[] SecondSubArr = new int[4];
            result = new int[arr_size * 2];
            //pass two 4-element sub arrays to asm (one from first array and one from second array)
            while (FirstArrIndex < arr_size - 1 || SecondArrIndex < arr_size - 1) //loop until there is still data in at least one of the arrays
            {
                if (FirstArrIndex < 4 && SecondArrIndex < 4) //for first two sub arrays
                {
                    FirstSubArr[currentSubIndex] = FirstAsmArr[FirstArrIndex];
                    SecondSubArr[arrayIndexThreshold - currentSubIndex - 1] = SecondAsmArr[SecondArrIndex];
                    FirstArrIndex++;
                    SecondArrIndex++;
                    currentSubIndex++;
                }
                else
                {
                    if (currentSubIndex == arrayIndexThreshold)
                        currentSubIndex--;
                    //sort elements
                    SortAsm.mergeSortASM(FirstSubArr, SecondSubArr);
                                            
                    //if statement preventing out of index read
                    if (FirstArrIndex + 1 == arr_size)
                        FirstAsmArr[FirstArrIndex] = Int32.MaxValue;

                    //check from which array should the next vector be taken from
                    //pass the lower vector as a result and swap it with new lowest array
                    if (FirstAsmArr[FirstArrIndex] < SecondAsmArr[SecondArrIndex] || SecondArrIndex + 1 == arr_size)
                    {
                        if (FirstSubArr[currentSubIndex] < SecondSubArr[currentSubIndex]) //a rare error can occur when last element is lower than last four
                        {
                            currentSubIndex = 0;
                            for (int j = 0; j < arrayIndexThreshold; j++)
                            {
                                result[currentIndex] = FirstSubArr[currentSubIndex];
                                currentSubIndex++;
                                currentIndex++;
                            }
                            for (int i = arrayIndexThreshold; i > 0; i--)
                            {
                                FirstSubArr[i - 1] = FirstAsmArr[FirstArrIndex];
                                if (FirstArrIndex < arr_size - 1)
                                    FirstArrIndex++;
                            }
                        }
                        else
                        {
                            currentSubIndex = 0;
                            for (int j = 0; j < arrayIndexThreshold; j++)
                            {
                                result[currentIndex] = SecondSubArr[currentSubIndex];
                                currentSubIndex++;
                                currentIndex++;
                            }
                            for (int i = arrayIndexThreshold; i > 0; i--)
                            {
                                SecondSubArr[i - 1] = FirstAsmArr[FirstArrIndex];
                                if (FirstArrIndex < arr_size - 1)
                                    FirstArrIndex++;
                            }
                        }
                    }
                    else
                    {
                        if (FirstSubArr[currentSubIndex] < SecondSubArr[currentSubIndex])
                        {
                            currentSubIndex = 0;
                            for (int j = 0; j < arrayIndexThreshold; j++)
                            {
                                result[currentIndex] = FirstSubArr[currentSubIndex];
                                currentSubIndex++;
                                currentIndex++;
                            }

                            for (int i = arrayIndexThreshold; i > 0; i--)
                            {
                                FirstSubArr[i - 1] = SecondAsmArr[SecondArrIndex];
                                if (SecondArrIndex < arr_size - 1)
                                    SecondArrIndex++;
                            }

                        }
                        else
                        {
                            currentSubIndex = 0;
                            for (int j = 0; j < arrayIndexThreshold; j++)
                            {
                                result[currentIndex] = SecondSubArr[currentSubIndex];
                                currentSubIndex++;
                                currentIndex++;
                            }
                            for (int i = arrayIndexThreshold; i > 0; i--)
                            {
                                SecondSubArr[i - 1] = SecondAsmArr[SecondArrIndex];
                                if (SecondArrIndex < arr_size - 1)
                                    SecondArrIndex++;
                            }
                        }
                    }
                }
            }
            //last two vectors are loaded
            SortAsm.mergeSortASM(FirstSubArr, SecondSubArr);
            //put last two vectors in 

            if (FirstSubArr[0] < SecondSubArr[0])
            {
                currentSubIndex = 0;
                for (int j = 0; j < arrayIndexThreshold; j++)
                {
                    result[currentIndex] = FirstSubArr[currentSubIndex];
                    currentSubIndex++;
                    currentIndex++;
                }
                currentSubIndex = 0;
                for (int j = 0; j < arrayIndexThreshold; j++)
                {
                    result[currentIndex] = SecondSubArr[currentSubIndex];
                    currentSubIndex++;
                    currentIndex++;
                }
            }
            else
            {
                currentSubIndex = 0;
                for (int j = 0; j < arrayIndexThreshold; j++)
                {
                    result[currentIndex] = SecondSubArr[currentSubIndex];
                    currentSubIndex++;
                    currentIndex++;
                }
                currentSubIndex = 0;
                for (int j = 0; j < arrayIndexThreshold; j++)
                {
                    result[currentIndex] = FirstSubArr[currentSubIndex];
                    currentSubIndex++;
                    currentIndex++;
                }
            }
        }

        //Function sorting an array using cpp code
        public void SortUsingCPP()
        {
            arr_size = CppArr.Length;
            SortCpp.mergeSort(CppArr, 0, arr_size - 1);
        }

        //Function used to display sorted elements in ASM array
        public String getContentsASM(String message)
        {
            String arrContents = message;
            for (int i = 0; i < arr_size * 2; i++)
            {
                arrContents += result[i];
                arrContents += " ";
            }
            return arrContents;
        }

        //Function used to display sorted elements in CPP array
        public String getContentsCPP(String message)
        {
            String arrContents = message;
            for (int i = 0; i < CppArr.Length; i++)
            {
                arrContents += CppArr[i];
                arrContents += " ";
            }
            return arrContents;
        }
    }
}
