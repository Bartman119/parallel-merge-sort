﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/*
 Topic: Merge Sort algorithm - execution time comparison using cpp and dll code
 Description: Program sorts randomly generated data using Merge Sort algorithms written in cpp and asm. Its size is determined by the user.  
 02.11.2020, 5th semester, year 2020/2021, Bartosz Groffik
 Current version: included in PZE folder
 Previous versions' functionalities:
 02.11.2020 - final version
 30.10.2020 - Refactoring and final version of UI
 29.10.2020 - Asm algorithm was created, optimized array filling and array containers
 28.10.2020 - 1st version of .asm project was created
 26.10.2020 - Universal data and container generation was added
 25.10.2020 - Data results moved to C# and resolved dll issues
 15.10.2020 - Merge sort C++ implementation was created
 12.10.2020 - 1st version of user UI was created
*/

namespace SorterUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public unsafe class SortCpp
        {
            [DllImport("MergeSortCpp.dll", CharSet = CharSet.Ansi, SetLastError = true)]
            public static extern int getMessageLength(int[] arr, int arr_size);

            [DllImport("MergeSortCpp.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void mergeSort(int[] arr, int l, int r);

        }

        public unsafe class SortAsm
        {
            [DllImport("BitonicMergeSortAsm.dll")]
            public static extern void DoSth(int[] x, int[] y);
        }

        private void StartSortingButton_Click(object sender, RoutedEventArgs e)
        {
            int NumberOfIntegers = 0;
            try
            {
                NumberOfIntegers = Int32.Parse(NumberOfIntegersTextBox.Text);
            }
            catch (FormatException fe)
            {
                Window popupErrorWindow = new Window();
                popupErrorWindow.Height = UIMainWindow.Height * 0.5;
                popupErrorWindow.Width = UIMainWindow.Width * 0.5;
                Label errorMessage = new Label();
                errorMessage.Content = "Data input error! Please input number of integers in correct format! \n" + fe.Message;
                popupErrorWindow.Content = errorMessage;
                popupErrorWindow.Show();
            }         

            //CONSTANTS
            const int INT_MAX = 65535;
            const int ARRAY_SIZE = 8;

            //creating new MergeSortController and resetting textboxes
            MergeSortController msc = new MergeSortController(INT_MAX, ARRAY_SIZE, NumberOfIntegers);
            SortedListTextBox.Text = "";
            SortedASMListTextBox.Text = "";
            TimeElapsedTextBox.Text = "";

            //generating and storing values in arrays
            SortedASMListTextBox.Text += "Generating random values... \n";
            SortedListTextBox.Text += "Generating random values... \n";
            msc.GenerateValues();
            SortedASMListTextBox.Text += "Done !\n";
            SortedListTextBox.Text += "Done !\n";

            //check if both algorithms have to run
            bool isComparisonEnabled = false;
            if(ComparisonModeSelected.IsChecked == true)
            {
                isComparisonEnabled = true;
            }

            //check for algorithm
            if (AsmCodeSelected.IsChecked == true || isComparisonEnabled)
            {
                var watchAsm = System.Diagnostics.Stopwatch.StartNew();

                //run sorting algorithm
                SortedASMListTextBox.Text += "Running the algorithm...\n";
                msc.SortUsingASM();
                SortedASMListTextBox.Text += "Done !\n";
                watchAsm.Stop();
                
                //get results
                var elapsedMs = watchAsm.ElapsedMilliseconds;
                if(IsOutputEnabled.IsChecked == false)
                {
                    String arrContents = msc.getContentsASM("Sorted array using asm: ");
                    SortedASMListTextBox.Text += arrContents + "\n";
                }

                String elapsedTime = elapsedMs.ToString();
                String message = "Execution time with asm: " + elapsedTime;
                TimeElapsedTextBox.Text += message + "\n";

                System.Diagnostics.Debug.WriteLine(message);
            }   
            if(CppCodeSelected.IsChecked == true || isComparisonEnabled)
            {
                var watchCpp = System.Diagnostics.Stopwatch.StartNew();

                //run sorting algorithm
                SortedListTextBox.Text += "Running the algorithm...\n";
                msc.SortUsingCPP();
                SortedListTextBox.Text += "Done !\n";
                watchCpp.Stop();
                
                //get results
                var elapsedMs = watchCpp.ElapsedMilliseconds;
                if(IsOutputEnabled.IsChecked == false)
                {
                    String arrContents = msc.getContentsCPP("Sorted array using cpp: ");
                    SortedListTextBox.Text += arrContents + "\n";
                }
                
                String elapsedTime = elapsedMs.ToString();
                String message = "Execution time with cpp: " + elapsedTime;
                TimeElapsedTextBox.Text += message + "\n";

            }

        }

        //Function changing flags of textboxes when CppCodeSelected is checked
        private void CppCodeSelected_Checked(object sender, RoutedEventArgs e)
        {
            AsmCodeSelected.IsChecked = false;
            ComparisonModeSelected.IsChecked = false;
        }

        //Function changing flags of textboxes when AsmCodeSelected is checked
        private void AsmCodeSelected_Checked(object sender, RoutedEventArgs e)
        {
            CppCodeSelected.IsChecked = false;
            ComparisonModeSelected.IsChecked = false;
        }

        //Function changing flags of textboxes when ComparisonModeSelected is checked
        private void ComparisonModeSelected_Checked(object sender, RoutedEventArgs e)
        {
            CppCodeSelected.IsChecked = false;
            AsmCodeSelected.IsChecked = false;
        }

        //Function changing flags of textboxes when IsOutputEnabled is checked
        private void IsOutputEnabled_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
