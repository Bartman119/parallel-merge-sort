#pragma once
#ifdef PARALLELSORTALGORITHM_EXPORTS
#define PARALLELSORTALGORITHM_API __declspec(dllexport)
#else
#define PARALLELSORTALGORITHM_API __declspec(dllimport)
#endif


extern "C" PARALLELSORTALGORITHM_API void merge(int arr[], int l, int m, int r);

extern "C" PARALLELSORTALGORITHM_API void mergeSort(int arr[], int l, int r);

extern "C" PARALLELSORTALGORITHM_API void printArray(int A[], int size);